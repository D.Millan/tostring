/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatostring;

/**
 *
 * @author D. Millan
 */
public class Magia {
    private String nome;
    private double consumo;
    
    public Magia(String nome, double consumo)
    {
        this.nome = nome;
        this.consumo = consumo;
    }
    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getConsumo() {
        return consumo;
    }

    public void setConsumo(double consumo) {
        this.consumo = consumo;
    }
    
    @Override
    public Magia clone()
    {
        return new Magia(nome,consumo);
    }
    
    @Override
    public String toString()
    {
        return " usou a magia " + nome + " consumindo " + consumo + " de mana";
    }
}

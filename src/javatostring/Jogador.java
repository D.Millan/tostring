/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatostring;

/**
 *
 * @author D. Millan
 */    
public class Jogador {
    private double vida;
    private double mana;
    private Magia[] magias;
    private String nome;
    private Inventario inventario;
    private int qtd = 0;

    public Jogador(String nome, double vida, double mana, int tamInv) {
        this.nome = nome;
        this.magias = new Magia[4];
        inventario = new Inventario(tamInv);
        this.vida = vida;
        this.mana = mana;
        for (int i=0; i<4; i++)
        {
            magias[i] = new Magia("", 0);
        }
    }

    public double getVida() {
        return vida;
    }

    public void setVida(double vida) {
        this.vida = vida;
    }

    public double getMana() {
        return mana;
    }

    public void setMana(double mana) {
        this.mana = mana;
    }

    public int getQtd() {
        return qtd;
    }


    public Magia[] getMagias() {
        return magias;
    }
    
    public boolean setMagia(Magia novaMagia)
    {
        if(qtd<4)
        {
            magias[qtd] = novaMagia;
            qtd++;
            return true;
        }
        return false;        
    }
    
    public boolean deletMagia(int posi)
    {
        if(posi<qtd && qtd>0)
        {
            qtd--;
            for(int i = posi; i<qtd; i++)
            {
                magias[i] = magias[i+1].clone();
            }
            magias[qtd].setNome("");
            return true;
        }
        return false;
    }
    
    public void setMagias(Magia[] magias) {
        this.magias = magias;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Inventario getInventario() {
        return inventario;
    }

    public void setInventario(Inventario inventario) {
        this.inventario = inventario;
    }
    
    public String executarMagia(int i)
    {
        if(i>=qtd)
        {
            return (nome + " não recebeu a "+(i+1)+"ª magia");
        }
        if(mana >= magias[i].getConsumo())
        {
            mana -= magias[i].getConsumo();
           return (nome+magias[i]); 
        }
        else
        {
            return (nome + " não usou a magia por falta de mana");
        }
    }
    public String usarItem(int i)
    {
        if(i <  inventario.getQtd())
        {
           return (nome + inventario.getItens()[i]); 
        }
        return (nome + " não recebeu o "+(i+1)+"º item");
    }
}

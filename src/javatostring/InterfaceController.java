/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatostring;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.ImageView;

/**
 *
 * @author D. Millan
 */
public class InterfaceController implements Initializable {
    
    @FXML
    private Label terminal, m1p1, m2p1, m3p1, m4p1, i1p1, i2p1, i3p1, i4p1, m1p2, m2p2, m3p2, m4p2, i1p2, i2p2, i3p2, i4p2;
    
    @FXML
    private TextField nome1, nome2, vida1, vida2, mana1, mana2, nMagia1, vMagia1, nItem1, vItem1, nMagia2, vMagia2, nItem2, vItem2;

    Jogador p1;
    Jogador p2;
    
    @FXML
    public void atualiza1(ActionEvent event)
    {
        p1.setNome(nome1.getText());
        p1.setVida(Double.parseDouble(vida1.getText()));
        p1.setMana(Double.parseDouble(mana1.getText()));
    }
    @FXML
    public void atualiza2(ActionEvent event)
    {
        p2.setNome(nome2.getText());
        p2.setVida(Double.parseDouble(vida2.getText()));
        p2.setMana(Double.parseDouble(mana2.getText()));
    }
    
    @FXML
    public void  novaM1(ActionEvent event)
    {
        if(p1.setMagia(new Magia(nMagia1.getText(),Double.parseDouble(vMagia1.getText()))))
        {
            terminal.setText("Magia adicionada com sucesso");
        }
        else
        {
            terminal.setText("Limite de magias atingido");
        }
        atualizarItens();
    }
    
    @FXML
    public void  novaM2(ActionEvent event)
    {
        if(p2.setMagia(new Magia(nMagia2.getText(),Double.parseDouble(vMagia2.getText()))))
        {
            terminal.setText("Magia adicionada com sucesso");
        }
        else
        {
            terminal.setText("Limite de magias atingido");
        }
        atualizarItens();
    }
    
    @FXML
    public void  novoI1(ActionEvent event)
    {
        if(p1.getInventario().setItem(new Item(nItem1.getText(),Double.parseDouble(vItem1.getText()))))
        {
            terminal.setText("Item adicionado com sucesso");
        }
        else
        {
            terminal.setText("Limite de itens atingido");
        }
        atualizarItens();
    }
    
    @FXML
    public void novoI2(ActionEvent event)
    {
        if(p2.getInventario().setItem(new Item(nItem2.getText(),Double.parseDouble(vItem2.getText()))))
        {
            terminal.setText("Item adicionado com sucesso");
        }
        else
        {
            terminal.setText("Limite de itens atingido");
        }
        atualizarItens();
    }
    
    @FXML
    public void removerMagia1(ActionEvent event){
        Button b = (Button) event.getTarget();
        if(p1.deletMagia(Integer.parseInt(b.getText().charAt(14)+"")-1))
        {
            terminal.setText("Magia removida com sucesso");
        }
        else
        {
            terminal.setText("Magia inexistente");
        }
        atualizarItens();
    }
    @FXML
    public void removerMagia2(ActionEvent event){
        Button b = (Button) event.getTarget();
        if(p2.deletMagia(Integer.parseInt(b.getText().charAt(14)+"")-1))
        {
            terminal.setText("Magia removida com sucesso");
        }
        else
        {
            terminal.setText("Magia inexistente");
        }
        atualizarItens();
    }
    @FXML
    public void usarMagia1(ActionEvent event){
        Button b = (Button) event.getTarget();
        terminal.setText(p1.executarMagia(Integer.parseInt(b.getText().charAt(11)+"")-1));
    }
    @FXML
    public void usarMagia2(ActionEvent event){
        Button b = (Button) event.getTarget();
        terminal.setText(p2.executarMagia(Integer.parseInt(b.getText().charAt(11)+"")-1));
    }
    @FXML
    public void removerItem1(ActionEvent event){
        Button b = (Button) event.getTarget();
        if(p1.getInventario().deletItem(Integer.parseInt(b.getText().charAt(13)+"")-1))
        {
            terminal.setText("Item removido com sucesso");
        }
        else
        {
            terminal.setText("Item inexistente");
        }
        atualizarItens();
    }
    @FXML
    public void removerItem2(ActionEvent event){
        Button b = (Button) event.getTarget();
        if(p2.getInventario().deletItem(Integer.parseInt(b.getText().charAt(13)+"")-1))
        {
            terminal.setText("Item removido com sucesso");
        }
        else
        {
            terminal.setText("Item inexistente");
        }
        atualizarItens();
    }
    public void usarItem1(ActionEvent event){
        Button b = (Button) event.getTarget();
        terminal.setText(p1.usarItem(Integer.parseInt(b.getText().charAt(10)+"")-1));
    }
    @FXML
    public void usarItem2(ActionEvent event){
        Button b = (Button) event.getTarget();
        terminal.setText(p2.usarItem(Integer.parseInt(b.getText().charAt(10)+"")-1));
    }
    
    public void atualizarItens()
    {
        m1p1.setText(p1.getMagias()[0].getNome());
        m2p1.setText(p1.getMagias()[1].getNome());
        m3p1.setText(p1.getMagias()[2].getNome());
        m4p1.setText(p1.getMagias()[3].getNome());
        
        m1p2.setText(p2.getMagias()[0].getNome());
        m2p2.setText(p2.getMagias()[1].getNome());
        m3p2.setText(p2.getMagias()[2].getNome());
        m4p2.setText(p2.getMagias()[3].getNome());
        
        i1p1.setText(p1.getInventario().getItens()[0].getNome());
        i2p1.setText(p1.getInventario().getItens()[1].getNome());
        i3p1.setText(p1.getInventario().getItens()[2].getNome());
        i4p1.setText(p1.getInventario().getItens()[3].getNome());
        
        i1p2.setText(p2.getInventario().getItens()[0].getNome());
        i2p2.setText(p2.getInventario().getItens()[1].getNome());
        i3p2.setText(p2.getInventario().getItens()[2].getNome());
        i4p2.setText(p2.getInventario().getItens()[3].getNome());
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        p1 = new Jogador(nome1.getText(), Double.parseDouble(vida1.getText()), Double.parseDouble(mana1.getText()), 4);
        p2 = new Jogador(nome2.getText(), Double.parseDouble(vida2.getText()), Double.parseDouble(mana2.getText()), 4);
    }    
    
}

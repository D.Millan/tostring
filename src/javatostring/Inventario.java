/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package javatostring;

/**
 *
 * @author D. Millan
 */
public class Inventario {
    private final int limite;
    private final Item[] itens;
    private int qtd = 0;

    
    public Inventario(int limite) {
        this.limite = limite;
        itens = new Item[limite];
        for(int i=0; i<limite; i++)
        {
         itens[i] = new Item("",0);
        }
    }
    
    public Item[] getItens() {
        return itens;
    }

    public int getLimite() {
        return limite;
    }

    public int getQtd() {
        return qtd;
    }
    
    public boolean setItem(Item novoItem)
    {
        if(qtd<limite)
        {
            itens[qtd] = novoItem;
            qtd++;
            return true;
        }
        return false;        
    }
    public boolean deletItem(int posi)
    {
        if(posi<qtd && qtd>0)
        {
            for(int i = posi; i<qtd-1; i++)
            {
                itens[i] = itens[i+1].clone();
            }
            qtd--;
            itens[qtd].setNome("");
            return true;
        }
        return false;
    }
    public int findItemNome(String nome)
    {
        for(int i = 0; i<qtd; i++)
        {
            if(nome.equals(itens[i].getNome())){
                return i;
            }
        }
        return -1;
    }
}

